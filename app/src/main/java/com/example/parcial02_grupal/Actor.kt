package com.example.parcial02_grupal

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.ForeignKey
import androidx.room.TypeConverter
import com.google.gson.Gson


@Entity(tableName = "actor")
data class Actor(

    @PrimaryKey val id: Int,
    @ColumnInfo(name = "first_name") val firstName: String,
    @ColumnInfo(name = "last_name") val lastName: String,
    @ColumnInfo(name = "born_day") val bornDay: Int,
    @ColumnInfo(name = "born_month") val bornMonth: Int,
    @ColumnInfo(name = "born_year") val bornYear: Int,
    /*
    @ColumnInfo(name = "awards") val awards: List<Awards>,
    @ColumnInfo(name = "movies") val movies: List<Movies>,
    */
    @ColumnInfo(name = "picture") val image: String,
    @ColumnInfo(name = "web") val web: String,
    @ColumnInfo(name = "instagram") val instagram: String
)

@Entity(foreignKeys = [ForeignKey(entity = Actor::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("actor_id"),
    onDelete = ForeignKey.CASCADE)]
)

data class Awards(val award: String)

data class Movies(val movie:String)


/*class AwardsTypeConverter {

    @TypeConverter
    fun listToJson(value: List<Awards>?) = Gson().toJson(value)
    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<Awards>::class.java).toList()
}*/

/*class MoviesTypeConverter {

    @TypeConverter
    fun listToJson(value: List<Movies>?) = Gson().toJson(value)
    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<Movies>::class.java).toList()
}*/
@Entity(tableName = "character")
data class Character(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "actorId") val actorId: Int,
    @ColumnInfo(name = "first_name") val firstName: String,
    @ColumnInfo(name = "last_name") val lastName: String,
    @ColumnInfo(name = "profession") val profession: String,
    @ColumnInfo(name = "role") val role: String,
    @ColumnInfo(name = "days_out_of_earth") val daysOutOfYear: Int
)



