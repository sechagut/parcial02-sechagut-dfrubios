package com.example.parcial02_grupal

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ActorDao {
    @Query("SELECT * FROM actor")
    fun getAll(): List<Actor>

    @Query("SELECT * FROM actor WHERE id = :id")
    fun getActor(id: Int): Actor

    @Query("SELECT * FROM actor WHERE id IN (:actorIds)")
    fun loadAllByIds(actorIds: IntArray): List<Actor>

    @Insert
    fun insert(actors: List<Actor>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertActor(actor: Actor)
}
