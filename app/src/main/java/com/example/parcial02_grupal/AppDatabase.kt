package com.example.parcial02_grupal

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters


@Database(entities = [Actor::class, Character::class], version = 1)
//@TypeConverters(MoviesTypeConverter::class, AwardsTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun actorDao(): ActorDao
    abstract  fun characterDao(): CharacterDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "The martian"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
