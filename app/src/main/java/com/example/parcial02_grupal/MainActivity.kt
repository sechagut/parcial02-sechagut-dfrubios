package com.example.parcial02_grupal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageButton
import android.widget.TextView
import com.example.parcial02_grupal.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {

    private lateinit var roomDatabase: AppDatabase

    //Definición de los textView
    private lateinit var actor0: TextView
    private lateinit var actor1: TextView
    private lateinit var actor2: TextView
    private lateinit var actor3: TextView
    private lateinit var actor4: TextView
    private lateinit var actor5: TextView
    private lateinit var actor6: TextView
    private lateinit var actor7: TextView

    //Definición de los botones

    private lateinit var actor1Button: ImageButton
    private lateinit var actor2Button: ImageButton
    private lateinit var actor3Button: ImageButton
    private lateinit var actor4Button: ImageButton
    private lateinit var actor5Button: ImageButton
    private lateinit var actor6Button: ImageButton
    private lateinit var actor7Button: ImageButton
    private lateinit var actor8Button: ImageButton

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    private fun setSupportActionBar(materialToolbar: Int) {
        TODO("Not yet implemented")
    }

    override fun onOptionsItemSelected(item: MenuItem)= when(item.itemId){
        R.id.desarrolladores ->{
            startActivity(Intent(this, desarrolladores::class.java))
            true
        }
        R.id.home ->{
            startActivity(Intent(this, MainActivity::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        roomDatabase = AppDatabase.getDatabase(this)

        writeData()
        writeDataCharacter()

        //Asignación de los textView
        actor0 = findViewById(R.id.prueba)
        actor1 = findViewById(R.id.prueba1)
        actor2 = findViewById(R.id.prueba2)
        actor3 = findViewById(R.id.prueba3)
        actor4 = findViewById(R.id.prueba4)
        actor5 = findViewById(R.id.prueba5)
        actor6 = findViewById(R.id.prueba6)
        actor7 = findViewById(R.id.prueba7)

        //Asignación de los botones
        actor1Button = findViewById(R.id.imagen01)
        actor2Button = findViewById(R.id.imagen02)
        actor3Button = findViewById(R.id.imagen03)
        actor4Button = findViewById(R.id.imagen04)
        actor5Button = findViewById(R.id.imagen05)
        actor6Button = findViewById(R.id.imagen06)
        actor7Button = findViewById(R.id.imagen07)
        actor8Button = findViewById(R.id.imagen08)

        //Llamado de los botones
        actor1Button.setOnClickListener {
            readData(1)
        }
        actor2Button.setOnClickListener {
            readData(2)
        }
        actor3Button.setOnClickListener {
            readData(3)
        }
        actor4Button.setOnClickListener {
            readData(4)
        }
        actor5Button.setOnClickListener {
            readData(5)
        }
        actor6Button.setOnClickListener {
            readData(6)
        }
        actor7Button.setOnClickListener {
            readData(7)
        }
        actor8Button.setOnClickListener {
            readData(8)
        }

    }

    fun writeData(){

        val actor =  Actor(1, "Sean", "Bean",
            17, 4, 1959, "a.jpg",
            "https://www.sensacine.com/actores/actor-7541/", "https://www.instagram.com/sean_bean_official/?hl=es")
        val actor2 =  Actor(2, "Jeff", "Daniels",
            19, 2, 1955, "b.jpg",
            "https://www.sensacine.com/actores/actor-1006/", "https://www.instagram.com/jeffdanielsofficial/")
        val actor3 =  Actor(3, "Kate", "Mara",
            27, 2, 1983, "c.jpg",
            "https://www.sensacine.com/actores/actor-84397/", "https://www.instagram.com/katemara/?hl=es")
        val actor4 =  Actor(4, "Michael", "Peña",
            13, 1, 1976, "d.jpg",
            "https://www.sensacine.com/actores/actor-92739/", "https://www.instagram.com/mvegapena/?hl=es")
        val actor5 =  Actor(5, "Jessica", "Chastain",
            24, 3, 1977, "e.jpg",
            "https://www.sensacine.com/actores/actor-117304/", "https://www.instagram.com/jessicachastain/?hl=es")
        val actor6 = Actor(6, "Sebastian", "Stan",
            13, 8, 1982, "f.jpg",
            "https://www.sensacine.com/actores/actor-115090/", "https://www.instagram.com/imsebastianstan/?hl=es")
        val actor7 = Actor(7, "Matt", "Damon",8	,
            10, 1970,  "g.jpg",
            "https://www.sensacine.com/actores/actor-1192/", "https://www.instagram.com/matt_damon/")
        val actor8 = Actor(8, "Kristen", "Wiig",
            22, 8, 1973, "h.jpg",
            "https://www.sensacine.com/actores/actor-178003/", "https://www.instagram.com/kristenwiigdaily/?hl=es")

        GlobalScope.launch(Dispatchers.IO) {
            roomDatabase.actorDao().insertActor(actor)
            roomDatabase.actorDao().insertActor(actor2)
            roomDatabase.actorDao().insertActor(actor3)
            roomDatabase.actorDao().insertActor(actor4)
            roomDatabase.actorDao().insertActor(actor5)
            roomDatabase.actorDao().insertActor(actor6)
            roomDatabase.actorDao().insertActor(actor7)
            roomDatabase.actorDao().insertActor(actor8)
        }
    }
    fun writeDataCharacter(){

        val personaje =  Character(1, 1, "Mitch",
            "Henderson", "Hermes Flight Director", "secundary", 0)
        val personaje2 =  Character(2, 2, "Teddy",
            "Sanders", "Nasa Director", "secundary", 0)
        val personaje3 = Character(3, 3, "Beth",
            "Johanseen", "Astronaut", "secundary", 400)
        val personaje4 =  Character(4, 4, "Rick",
            "Martinez", "Pilot", "secundary", 400)
        val personaje5 =  Character(5, 5, "Melissa",
            "Lewis", "Astronaut", "secundary", 400)
        val personaje6 =Character(6, 6, "Chris",
            "Beck", "Astronaut, Doctor", "secundary", 400)
        val personaje7 = Character(7, 7, "Mark",
            "Watney", "Astonaut, Biologist", "secundary", 1000)
        val personaje8 = Character(8, 8, "Annie",
            "Montrose", "NASA PR Director", "secundary", 0)

        GlobalScope.launch(Dispatchers.IO) {
            roomDatabase.characterDao().insertCharacter(personaje)
            roomDatabase.characterDao().insertCharacter(personaje2)
            roomDatabase.characterDao().insertCharacter(personaje3)
            roomDatabase.characterDao().insertCharacter(personaje4)
            roomDatabase.characterDao().insertCharacter(personaje5)
            roomDatabase.characterDao().insertCharacter(personaje6)
            roomDatabase.characterDao().insertCharacter(personaje7)
            roomDatabase.characterDao().insertCharacter(personaje8)
        }
    }

    private suspend fun display(actor: Actor){
        withContext(Dispatchers.Main){
            actor0.text = actor.firstName
            actor1.text = actor.lastName
            actor2.text = actor.bornDay.toString()
            actor3.text = actor.bornMonth.toString()
            actor4.text = actor.bornYear.toString()
            actor5.text = actor.image
            actor6.text = actor.web
            actor7.text = actor.instagram
        }
    }

    fun readData(id:Int){
        lateinit var actor: Actor

        GlobalScope.launch {

            actor = roomDatabase.actorDao().getActor(id)
            display(actor)
        }
    }
}
