package com.example.parcial02_grupal

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CharacterDao
{
    @Query("SELECT * FROM character")
    fun getAll(): List<Character>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(character: List<Character>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertCharacter(characters: Character)

    @Query("SELECT * FROM character WHERE id IN (:characterIds)")
    fun loadAllByIds(characterIds: IntArray): List<Character>

}